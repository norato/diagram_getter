function ClassDiagram (className, attrs, methods) {
  this.name = className;
  this.attrs = attrs;
  this.methods = methods;
}

ClassDiagram.prototype.drawClass = function(local) {
  
  divLocation = local || "Diagram_Getter";
  divParent = $("#"+divLocation);

  divParent.append("<div class='classElement' id='"+this.name+"'></div>");
  nomediv = "#"+ divParent[0].id +" > #"+this.name+""
  div = $(nomediv);
  div.append("<div id='title'>"+this.name+"</div>");
  div.append("<div class='classAttrs' id='"+this.name+"Attrs'></div>");
  div.append("<div class='classMethods' id='"+this.name+"Methods'></div>");

  for (attr in this.attrs){
    this.addAttr(this.attrs[attr], nomediv);
  }

  for (method in this.methods){
    this.addMethod(this.methods[method], nomediv);
  }
};

ClassDiagram.prototype.addAttr = function(arrayAttr, div){
  var view = arrayAttr[0];
  var attrName = arrayAttr[1];
  var type = arrayAttr[2];
  var text = view +" " + attrName +": " + type;
  $(div +" .classAttrs").append("<div class=attr id='"+attrName+"'>"+text+"</div>")
};

ClassDiagram.prototype.addMethod = function(arrayMethod, div) {
  var view = arrayMethod[0];
  var methodName = arrayMethod[1];
  var type = arrayMethod[2];
  var text = view +" " + methodName +"(): " + type;
  $(div +" .classMethods").append("<div class=method id='"+methodName+"'>"+text+"</div>")
};

//problemas com ID

ClassDiagram.prototype.leftPosition = function(local) {
  this.div = $("#"+local +"> #"+this.name+"");
  return {
    x : this.div.offset().left,
    y : this.div.offset().top + (this.div.height()/2)
  };
};

ClassDiagram.prototype.rightPosition = function(local) {
  this.div = $("#"+local +"> #"+this.name+"");
  return {
    x : this.div.offset().left + this.div.width(),
    y : this.div.offset().top + (this.div.height()/2)
  };
};


ClassDiagram.prototype.associateWith = function(other_class) {
  var id = this.name + "Have" + other_class.name;
  var idDiv = id + "div"
  
  $("#Diagram_Getter").append("<div class='association' id='"+idDiv+"'></div>");

  this.drawClass(idDiv);
  other_class.drawClass(idDiv);

  var thisClassX = this.rightPosition(idDiv).x
  var thisClassY = this.rightPosition(idDiv).y
  var otherClassX = other_class.leftPosition(idDiv).x
  var otherClassY = other_class.leftPosition(idDiv).y
  




  $("#"+idDiv+"").append("<hr id='"+id+"'class='line'>");

  line = $("#"+id+"");
  line.css({
    "left"  : (thisClassX + 4)+"px",
    "top"   : thisClassY+"px",
    "width" : "63px",
    "border-width": "3px"

  });

};