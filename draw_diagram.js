$(document).ready(function(){

  diagram = new ClassDiagram("ClassName",
                                    //  Attrs 
                                    [["public", "attr1", "int"],
                                     ["public", "attr2", "int"],
                                     ["public", "attr3", "int"]],
                                    // Methods  
                                    [["public", "method1", "int"],
                                     ["public", "method2", "int"],
                                     ["public", "method3", "int"]]);

  diagram2 = new ClassDiagram("ClassName2",
                                    //  Attrs 
                                    [["public", "attr1", "int"],
                                     ["public", "attr2", "int"],
                                     ["public", "attr3", "int"]],
                                    // Methods  
                                    [["public", "method1", "int"],
                                     ["public", "method2", "int"],
                                     ["public", "method3", "int"]]);


  diagram.associateWith(diagram2);

  diagram3 = new ClassDiagram("ClassName3",
                                    //  Attrs 
                                    [["public", "attr1", "int"],
                                     ["public", "attr2", "int"],
                                     ["public", "attr3", "int"]],
                                    // Methods  
                                    [["public", "method1", "int"],
                                     ["public", "method2", "int"],
                                     ["public", "method3", "int"]]);

  diagram4 = new ClassDiagram("ClassName4",
                                    //  Attrs 
                                    [["public", "attr1", "int"],
                                     ["public", "attr2", "int"],
                                     ["public", "attr3", "int"]],
                                    // Methods  
                                    [["public", "method1", "int"],
                                     ["public", "method2", "int"],
                                     ["public", "method3", "int"]]);

  diagram2.associateWith(diagram3); 

  diagram4.associateWith(diagram3);  

  
  diagram2.drawClass();

});