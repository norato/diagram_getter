describe('Associate Two Classes', function(){
  var class1;
  var class1_div;
  var class2;
  var class2_div;

  beforeEach(function(){
    setFixtures('<div id="Diagram_Getter" />');
    this.class1 = new ClassDiagram("Class1");
    this.class2 = new ClassDiagram("Class2");

    this.class1.associateWith(this.class2);

    this.class1_div = $("#Class1");
    this.class2_div = $("#Class2");
  });

  it("Calculate the left position", function(){
    var x = this.class1_div.offset().left;
    var y = this.class1_div.offset().top + (this.class1_div.height() / 2);
    expect(this.class1.leftPosition("Class1HaveClass2div").x).toBe(x);
    expect(this.class1.leftPosition("Class1HaveClass2div").y).toBe(y);
  });

  it("Calculate the right position", function(){
    var x = this.class1_div.offset().left + this.class1_div.width();
    var y = this.class1_div.offset().top + (this.class1_div.height() / 2);
    expect(this.class1.rightPosition("Class1HaveClass2div").x).toBe(x);
    expect(this.class1.rightPosition("Class1HaveClass2div").y).toBe(y);
  });

  it("The Classes will be associate by horizontal line", function() {
    expect($("#Diagram_Getter")).toContain($(".line"));
  });
  
  it("The canvas div will be between its classes",function(){
    var line_left = $(".line")[0].offsetLeft;
    var class_right_mod =  this.class1.rightPosition("Class1HaveClass2div").x + 4;
    expect(line_left).toEqual(class_right_mod);
  });

});

describe("Associate More Classes", function() {
  var class1;
  var class1_div;
  
  var class2;
  var class2_div;
  
  var class3;
  var class3_div;

  beforeEach(function() {
    setFixtures('<div id="Diagram_Getter" />');
    
    this.class1 = new ClassDiagram("Class1");
    this.class1.drawClass();
    this.class1_div = $("#Class1");

    this.class2 = new ClassDiagram("Class2");
    this.class2.drawClass();
    this.class2_div = $("#Class2");

    this.class3 = new ClassDiagram("Class3");
    this.class3.drawClass();
    this.class3_div = $("#Class3");
  });

  it("Three Associations", function() {
    this.class1.associateWith(this.class2);
    this.class2.associateWith(this.class3);
  });
});