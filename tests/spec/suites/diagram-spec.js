describe('Create Classes', function(){
  var diagram;
  beforeEach(function(){
    setFixtures('<div id="Diagram_Getter" />');
    this.diagram = new ClassDiagram("ClassName",
                                    //  Attrs 
                                    [["public", "attr1", "int"],
                                     ["public", "attr2", "int"],
                                     ["public", "attr3", "int"]],
                                    // Methods  
                                    [["public", "method1", "int"],
                                     ["public", "method2", "int"],
                                     ["public", "method3", "int"]]);
    this.diagram.drawClass();
  });

  it("The page should have a div id 'Diagram_Getter'", function(){
    expect($('#Diagram_Getter')).toExist();
  });

  it("When the class is setted the page should have a div class's name", function(){
    expect($("#ClassName")).toExist();
    expect($('#Diagram_Getter')).toContain($("#ClassName"));
  });

  it("Add the name for the class", function(){
    expect($("#title")).toExist();
    expect($("#ClassName")).toContain($("#title"));
    expect("ClassName").toBe($("#title").text());  
  });

  it("The name of classe should be at the center", function(){
    title = $("#title");
    expect(title).toHaveCss({"text-align": "center"});
  });

  it("The attributes have a div", function (){
    expect($("#ClassNameAttrs")).toExist();
    expect($("#ClassName")).toContain($("#ClassNameAttrs"));
  })

  it("The Class borders should be greater than attr div", function(){
    expect($(".classElement").width()).toEqual($(".classAttrs").width());
    expect($(".classElement").height()).toBeGreaterThan($(".classAttrs").height());
  });

  it("The methods have a div", function(){
    expect($("#ClassNameMethods")).toExist();
    expect($("#ClassName")).toContain($("#ClassNameMethods"));
  });

  it("The Class borders should be greater than methods div", function(){
    expect($(".classElement").width()).toEqual($(".classMethods").width());
    expect($(".classElement").height()).toBeGreaterThan($(".classMethods").height());
  });

  describe("Check methods and attributes", function() {
    
    it("Attrs", function() {
      expect("public attr1: int").toBe($("#ClassName #attr1").text());
      expect("public attr2: int").toBe($("#ClassName #attr2").text());
      expect("public attr3: int").toBe($("#ClassName #attr3").text());
    });

    it("Methods", function() {
      expect("public method1(): int").toBe($("#ClassName #method1").text());
      expect("public method2(): int").toBe($("#ClassName #method2").text());
    });
  });

});