describe('Private Methods', function(){
  var diagram;
  beforeEach(function(){
    setFixtures('<div id="Diagram_Getter" />');
    this.diagram = new ClassDiagram("ClassName");
    this.local = "#Diagram_Getter";
    this.diagram.drawClass();
  });

  it("Adding more attributes", function() {
    var attr1 = ["public", "attr1", 'int'];
    var attr2 = ["private", "attr2", 'String'];
    var attr3 = ["protected", "attr3", 'Boolean'];
    this.diagram.addAttr(attr1, this.local);
    this.diagram.addAttr(attr2, this.local);
    this.diagram.addAttr(attr3, this.local);
    expect("public attr1: int").toBe($("#ClassName #attr1").text());
    expect("private attr2: String").toBe($("#ClassName #attr2").text());
    expect("protected attr3: Boolean").toBe($("#ClassName #attr3").text());
  });

  it("Each class has its own attributes", function() {
    var attr1 = ["public", "attr1", 'int'];
    this.diagram.addAttr(attr1, this.local);
    expect("public attr1: int").toBe($("#ClassName #attr1").text());

    var otherClass = new ClassDiagram("OtherClass");
    otherClass.drawClass();
    var attr1_other = ["public", "attr1_other", 'int'];
    otherClass.addAttr(attr1_other, this.local);
    expect("public attr1_other: int").toBe($("#OtherClass #attr1_other").text());
    
    expect("public attr1_other: int").not.toBe($("#ClassName #attr1").text());
    expect("public attr1: int").not.toBe($("#OtherClass #attr1_other").text());
  });


  it("Adding more methods", function(){
    var method1 = ["public", "method1", 'int'];
    var method2 = ["public", "method2", 'int'];
    this.diagram.addMethod(method1, this.local);
    this.diagram.addMethod(method2, this.local);
    expect("public method1(): int").toBe($("#ClassName #method1").text());
    expect("public method2(): int").toBe($("#ClassName #method2").text());
  });

  it("Each class has its own methods", function() {
    var method1 = ["public", "method1", 'int'];
    var method2 = ["public", "method2", 'int'];
    this.diagram.addMethod(method1, this.local);
    this.diagram.addMethod(method2, this.local);
    expect("public method1(): int").toBe($("#ClassName #method1").text());
    expect("public method2(): int").toBe($("#ClassName #method2").text());

    var otherClass = new ClassDiagram("OtherClass");
    otherClass.drawClass();
    
    var method1_other = ["public", "method1_other", 'int'];
    var method2_other = ["public", "method2_other", 'int'];
    otherClass.addMethod(method1_other, this.local);
    otherClass.addMethod(method2_other, this.local);

    expect("public method1_other(): int").toBe($("#OtherClass #method1_other").text());
    expect("public method2_other(): int").toBe($("#OtherClass #method2_other").text());

    expect("public method2_other(): int").not.toBe($("#ClassName #method2").text());
    expect("public method2(): int").not.toBe($("#OtherClass #method2_other").text());
  });
});